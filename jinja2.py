from flask import Flask, render_template

app = Flask(__name__)

@app.route ('/')
def saludar():
    return 'Hola, esto es un dicionario de Slang Panameño!'

@app.route ("/")
def index():
    titulo = "dicionario"
    palabra = ["xopa","mopri","parking","yala vida"]
    significado = ["saludo","primo","fiesta","asombro"]
    return render_template("index.html", titulo=titulo, palabra=palabra, significadi=significado) 

if __name__=="__main__":
    app.run(debug=True)



